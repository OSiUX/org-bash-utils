#!/bin/bash

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2021 Osiris Alejandro Gomez <osiux@osiux.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2129
# shellcheck disable=SC2086

[[ -n "$BASH_DEBUG" ]] && set -x
[[ -e "$1"          ]] || exit 1
[[ -z "$1"          ]] && exit 1

ORG="$1"
MD="/tmp/$(basename "$ORG" .org).md"
TMP="/tmp/$(basename "$ORG" .org).tmp"
HDR="$(basename "$ORG" .org).hdr"
LINKS='-l at-end'

true > "$TMP"

[[ "$ORG" = 'index.org' ]] && LINKS=''
[[ "$ORG" = 'links.org' ]] && LINKS=''

if [[ -e "$HDR" ]]
then
  echo "#+BEGIN_EXAMPLE"           >> "$TMP"
  cat  "$HDR"                      >> "$TMP"
  echo "#+END_EXAMPLE"             >> "$TMP"
  echo ""                          >> "$TMP"
  echo "[[file:index.gmi][index]]" >> "$TMP"
  echo "[[file:osiux.gmi][osiux]]" >> "$TMP"
  echo "[[file:links.gmi][links]]" >> "$TMP"
  echo "[[file:tty.gmi][tty]]"     >> "$TMP"
  echo ""                          >> "$TMP"
fi

grep -v '{{{modification-time' "$ORG"     \
  | grep -v '^#+LANGUAGE:'                \
  | grep -v '^#+INCLUDE:'                 \
  | grep -v '^#+LINK_HOME:'               \
  | grep -v '^#+DESCRIPTION:'             \
  | grep -v '^#+HTML_HEAD:'               \
  | grep -v '^#+KEYWORDS:'                \
  | sed 's/\.org/.gmi/g'                  \
  | sed 's/^#+TITLE:\s\+/* /g'            \
  | sed "s/^#+AUTHOR:\s/\n\n- AUTHOR: /g" \
  | sed "s/^#+EMAIL:\s/- EMAIL: /g"       \
  | sed "s/^#+DATE:\s/- DATE: /g"         >> "$TMP"

PD_VERSION="$(pandoc -v | head -1 | grep -Eo '[0-9\.]+')"
PD_ATX='--atx-headers'
[[ "$PD_VERSION" = 2.19.2 ]] && PD_ATX='--markdown-headings=atx'

pandoc -f org -t "$TO" "$PD_ATX" \
  && sed -i 's/{.verbatim}//g' "$MD" \
  && md2gemini -i 2 -w -m $LINKS "$MD" && rm -f "$TMP"
